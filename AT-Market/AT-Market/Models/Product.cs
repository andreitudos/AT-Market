﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace AT_Market.Models
{
    public class Product
    {
        [Key]
        public int ProductID { get; set; }

        [Display(Name ="Descrição")]
        [StringLength(30,ErrorMessage ="A {0} deverá ter entre {2} e {1} carateres", MinimumLength = 3)]
        [Required(ErrorMessage ="Deve inserir uma {0}")]
        public string Description { get; set; }

        [Display(Name ="Preço")]
        [DataType(DataType.Currency)]
        [DisplayFormat(DataFormatString ="{0:C2}",ApplyFormatInEditMode =false)]
        [Required(ErrorMessage ="Deve inserir um {0}")]
        public decimal Price { get; set; }

        [Display(Name ="Ultima Compra")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString ="{0:dd/mm/yyyy}",ApplyFormatInEditMode =true)]
        public DateTime LastBuy { get; set; }

        public float Stock { get; set; }

        public string Image { get; set; }

        [NotMapped]
        [Display(Name = "Imagem")]
        public HttpPostedFileBase ProductImageURL { get; set; }

        [DataType(DataType.MultilineText)]
        public string Remarks { get; set; }

        //[Required(ErrorMessage = "A {0} e óbrigatoria")]
        //[Range(1, double.MaxValue, ErrorMessage = "Tem que selecionar uma {0}")]
        //[Display(Name = "Categoria")]
        public int CategoryID { get; set; }

        public virtual Category Category { get; set; }

        public virtual ICollection<SupplierProduct> SupplierProducts { get; set; }

        public virtual ICollection<OrderDetail> OrderDetails { get; set; }

    }
}
﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace AT_Market.Models
{
    public class Category
    {
        [Key]
        public int CategoryID { get; set; }

        [Display(Name = "Descrição")]
        [StringLength(100, ErrorMessage = "A {0} deverá ter entre {2} e {1} carateres", MinimumLength = 3)]
        [Required(ErrorMessage = "Deve inserir uma {0}")]
        public string Description { get; set; }

        public virtual ICollection<Product> Products { get; set; }
    }
}
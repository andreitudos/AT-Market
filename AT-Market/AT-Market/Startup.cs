﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(AT_Market.Startup))]
namespace AT_Market
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}

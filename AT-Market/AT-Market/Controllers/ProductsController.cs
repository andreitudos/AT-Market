﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using AT_Market.Helpers;
using AT_Market.Models;

namespace AT_Market.Controllers
{
    public class ProductsController : Controller
    {
        private AT_MarketContext db = new AT_MarketContext();

        // GET: Products
        [Authorize(Roles="View")]
        public ActionResult Index()
        {
            var products = db.Products.Include(c => c.Category);
            return View(products.ToList());
        }

        // GET: Products/Details/5
        [Authorize(Roles = "View")]
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Product product = db.Products.Find(id);
            if (product == null)
            {
                return HttpNotFound();
            }
            return View(product);
        }

        // GET: Products/Create
        [Authorize(Roles = "Create")]
        public ActionResult Create()
        {
            ViewBag.CategoryID = new SelectList(CombosHelper.GetCategories(), "CategoryID", "Description");
            return View();
        }

        // POST: Products/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ProductID,Description,Price,LastBuy,Stock,Image,ProductImageURL,CategoryID,Remarks")] Product product)
        {
            if (ModelState.IsValid)
            {
                db.Products.Add(product);
                db.SaveChanges();

                if (product.ProductImageURL != null)
                {
                    var folder = "~/Content/Images";
                    var file = string.Format("{0}.png",product.ProductID);

                    var response = FilesHelper.UploadImage(product.ProductImageURL, folder, file);

                    if (response)
                    {
                        var pic = string.Format("{0}/{1}", folder, file);
                        product.Image = pic;
                        db.Entry(product).State = EntityState.Modified;
                        db.SaveChanges();

                    }
                }

                return RedirectToAction("Index");
            }

            ViewBag.CategoryID = new SelectList(db.Categories, "CategoryID", "Description", product.CategoryID);
           // ViewBag.CategoryID = new SelectList(CombosHelper.GetCategories(), "CategoryID", "Description", product.CategoryID);

            return View(product);

        }

        // GET: Products/Edit/5
        [Authorize(Roles = "Edit")]
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Product product = db.Products.Find(id);
            if (product == null)
            {
                return HttpNotFound();
            }

            ViewBag.CategoryID = new SelectList(CombosHelper.GetCategories(), "CategoryID", "Description", product.CategoryID);
            return View(product);
        }

        // POST: Products/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ProductID,Description,Price,LastBuy,Stock,Image,ProductImageURL,CategoryID,Remarks")] Product product)
        {
            if (ModelState.IsValid)
            {
                if (product.ProductImageURL != null)
                {
                    var pic = string.Empty;
                    var folder = "~/Content/Images";
                    var file = string.Format("{0}.png", product.ProductID);

                    var response = FilesHelper.UploadImage(product.ProductImageURL, folder, file);

                    if (response)
                    {
                        pic = string.Format("{0}/{1}", folder, file);
                        product.Image = pic;                 
                    }
                }

                db.Entry(product).State = EntityState.Modified;
                db.SaveChanges();

                return RedirectToAction("Index");
            }

            ViewBag.CategoryID = new SelectList(CombosHelper.GetCategories(), "CategoryID", "Description", product.CategoryID);
            return View(product);
        }

        // GET: Products/Delete/5
        [Authorize(Roles = "Delete")]
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Product product = db.Products.Find(id);
            if (product == null)
            {
                return HttpNotFound();
            }
            return View(product);
        }

        // POST: Products/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Product product = db.Products.Find(id);
            db.Products.Remove(product);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}

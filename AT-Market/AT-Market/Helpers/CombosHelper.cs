﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AT_Market.Models;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;

namespace AT_Market.Helpers
{
    public class CombosHelper:IDisposable
    {
        private static AT_MarketContext db=new AT_MarketContext();
        private static ApplicationDbContext da = new ApplicationDbContext();

        public static List<DocumentType> GetDocumentTypes()
        {
            var DocumentTypes = db.DocumentTypes.ToList();
            DocumentTypes.Add(new DocumentType
            {
                DocumentTypeID=0,
                Description = "[Seleciona um tipo de documento...]"
            });

            return DocumentTypes.OrderBy(d=>d.Description).ToList();
        }

        public static List<Customer> GetCustomersNames()
        {
            var Customers = db.Customers.ToList();

            Customers.Add(new Customer
            {
                CustomerID = 0, 
                FirstName = "[Seleciona um cliente]"
            });

            return Customers.OrderBy(c => c.Name).ToList();
        }

        public static List<Product> GetProducts()
        {
            var Products = db.Products.ToList();
            Products.Add(new Product
            {
                ProductID = 0,
                Description = "[Seleciona um produto...]"
            });

            return Products.OrderBy(p => p.Description).ToList();
        }

        public static List<Category> GetCategories()
        {
            var Categories = db.Categories.ToList();
            Categories.Add(new Category
            {
                CategoryID = 0,
                Description = "[Seleciona uma Categoria...]"
            });

            return Categories.OrderBy(c => c.Description).ToList();
        }

        public static List<IdentityRole> GetRoles()
        {
            var roleManager = new RoleManager<IdentityRole>(new RoleStore<IdentityRole>(da));
            var list = roleManager.Roles.ToList();
            list.Add(new IdentityRole { Id = "", Name = "[Seleciona uma permissão]" });

            return list = list.OrderBy(r => r.Name).ToList();
        }
        public void Dispose()
        {
            db.Dispose();
        }
    }
}